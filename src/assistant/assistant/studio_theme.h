#pragma once

#include <QColor>
#include <QMargins>

const static QMargins NO_MARGINS{ 0, 0, 0, 0 };

const static int TITLE_SPACING(20);
const static int TITLE_FONT_SIZE(20);
const static int WIDGET_SPACING(10);
const static int SMALL_TITLE_FONT_SIZE(18);
const static int MIN_STUDIO_LABEL_WIDTH(150);
const static int COMBOBOX_WIDTH(200);
const static int BUTTON_WIDTH(150);

const static QColor DARK_THEME_DARK_GRAY_BACKGROUND_COLOR(0, 0, 0, 55);
const static QColor DARK_THEME_MAIN_BACKGROUND_COLOR(35, 35, 35);
const static QColor DARK_THEME_MAIN_BASE_COLOR(25, 25, 25);
const static QColor DARK_THEME_MAIN_BASE_ALTERNATE_COLOR(30, 30, 30);
const static QColor DARK_THEME_ITEM_ENABLED(30, 30, 30);
const static QColor DARK_THEME_ITEM_DISABLED(25, 25, 25);
const static QColor DARK_THEME_ITEM_HIGHLIGHTED(68, 65, 55);
const static QColor DARK_THEME_FRAME_BORDER(60, 60, 60);
const static QColor DARK_THEME_TEXT_ENABLED(200, 200, 200);
const static QColor DARK_THEME_TEXT_DISABLED(95, 95, 95);
const static QColor DARK_THEME_ACCENT_COLOR(204, 178, 93);

// Miro
const static QColor SEXY_RED_FOR_BUTTONS(128, 27, 25);
const static QColor SEXY_BLUE_FOR_BUTTONS(25, 37, 128);
const static QColor SEXY_GREEN_FOR_BUTTONS(25, 128, 49);
const static QColor SEXY_YELLOW_FOR_BUTTONS(161, 154, 29);
